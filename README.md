# flow analyze

This code is used for post-processing of CFD simulations.

Currently implemented are some wall shear stress descriptors. The aim is to also implement bulk flow descriptors, such as the localized normalized helicity (LNH) and maybe h1 to h4 from the papers of Gallo.

All processing is done in Python 3 (will probably also work with Python 2 after editing some of the print statements, but not all functionality is ensured).
Because the XDMF Python API is not used (too much hassle to compile, especially on multiple machines) all data reading/writing is done via HDF (h5py) and xml (xml.etree.ElementTree and xml.dom.minidom). Consequently, a limited amount of functionality of XDMF is implemented: topology, geometry and data is assumed to be in a HDF5 file (binary and XML is not supported). An overview of the XML part of XDMF is presented in xdmf_layout.txt
Some metrics are computed directly, for other quantities VTK is used.

The following python packages are mandatory and can be installed with pip install PACAKGE (--user) (python2) or pip3 install PACAKGE (--user) (python3):
h5py
xml
numpy
vtk
