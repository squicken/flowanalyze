#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Script for easily finding the start en end of the last beat
"""

# Import statements
import numpy as np  # For reading csv files
import peakutils  # processing peak data get with pip3 isntall peakutils
import matplotlib.pyplot as plt  # for plotting
from argparse import ArgumentParser  # Parse the command line

__author__ = 'Sjeng Quicken work PC'
__email__ = 's.quicken@maastrichtuniversity.nl'
__date__ = '2018-04-24'

# Last Modified by:   Sjeng Quicken work PC
# Last Modified time: 2018-04-24

parser = ArgumentParser()

# Input file
parser.add_argument(
    '-i', '--input', type=str,
    help='name of the file to analyze')

# Input file
parser.add_argument(
    '-p', '--plot', action='store_true',
    help='Plot the results (default is False)')

# Parse the arguments
arguments = parser.parse_args()

infile = arguments.input
plot = arguments.plot

# Load the csvfile
data = np.genfromtxt(infile, delimiter=',')

# The inflow data (that is needed for determining the last beat) is stored in
# collumn 1. The first row contains headers. Note that flow is inverted
inflow = data[1:, 0]

# Do some peak detecting
indexes = peakutils.indexes(
    inflow,  # Data array
    thres=np.max(0.2 * np.abs(inflow))  # Threshold
)

begin = indexes[-2]
end = indexes[-1]

print('[{:d}, {:d}]'.format(begin, end))

if plot:
    plt.plot(inflow)
    plt.scatter(indexes, inflow[indexes])
    plt.scatter(begin, inflow[begin], color='r')
    plt.scatter(end, inflow[end], color='c')
    plt.show()
