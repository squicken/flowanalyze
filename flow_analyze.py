#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This program is used to compute bulk flow parameters from CFD simulations

Velocity data over time should be available in XDMF file format

Helpfull examples
https://stackoverflow.com/questions/45665130/create-mesh-from-cells-and-points-in-paraview
https://www.paraview.org/Wiki/ParaView/Simple_ParaView_3_Python_Filters#Producing_An_Unstructured_Grid_.28Source.29
"""

# Import statements
import xdmf  # Custom class for importing XDMF datasets
from paraview import vtk, simple
import numpy as np


__author__ = 'Sjeng Quicken work PC'
__email__ = 's.quicken@maastrichtuniversity.nl'
__date__ = '2018-04-26'

# Last Modified by:   Sjeng Quicken work PC
# Last Modified time: 2018-04-26

# Read the XDMF file containing the velocity data
xdmf_in = xdmf.Xdmf(
    filename='u_from_tstep_0.xdmf',
    mode='r'  # Read data
)

# Construct the structures for storing the mesh in VTK
dataset = vtk.vtkPolyData()
pts = vtk.vtkPoints()

# Loop over the points and cells in the XDMF and add to VTK
point_data = xdmf_in.geometry_data[1]
cell_data = xdmf_in.topology_data[1]

# Extract the number of points and the number of cells
num_points = point_data.shape[0]
num_cells = cell_data.shape[0]

# Add the points to the datset
for ipoint in range(num_points):
    x = cell_data[ipoint, 0]
    y = cell_data[ipoint, 1]
    z = cell_data[ipoint, 2]
    pts.InsertNextPoint(x, y, z)

dataset.SetPoints(pts)

# Allocate the number of cells (probably faster?)
dataset.Allocate(
    num_cells,  # The number of cells
    1000)  # The extra allocated size

# Number of points per cell (all tetra)
num_cell_points = 4

# Loop over all cells
for icell in range(num_cells):
    # Initialize a cell
    cell = vtk.vtkTetra()
    point_ids = cell.GetPointIds()
    for point_id in range(num_cell_points):
        # Add the point ids to the cell
        point_ids.SetId(point_id, cell_data[icell, point_id])
    # Add the cell to the dataset.
    dataset.InsertNextCell(10, cell.GetPointIds())
