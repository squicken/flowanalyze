#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Used for computing WSS from CFD simulations

WSS over time should be available in XDMF file format """

# Import statements
from __future__ import print_function  # For printing
import xdmf  # Custom class for importing XDMF datasets
import numpy as np
from pdb import set_trace as bp  # For debugging

__author__ = 'Sjeng Quicken work PC'
__email__ = 's.quicken@maastrichtuniversity.nl'
__date__ = '2018-04-23'

# Last Modified by:   Sjeng Quicken work PC
# Last Modified time: 2018-04-26


def extract_wss(xdmf_in):
    """ Used to extract the wall shear stress from the time range"""

    time_steps = xdmf_in.time_steps
    roi = xdmf_in.data_roi

    # Extract the HDF location
    wss_hdf = time_steps[roi[0]].find(
        'Attribute'
    ).find(
        'DataItem'
    ).text.split(':')

    # Print the name of the dataset
    print(('Reading {:s}:{:s}'.format(wss_hdf[0], wss_hdf[1])), end='')

    # Load the first dataset
    wss_data = xdmf_in.hdf5_read(
        filename=wss_hdf[0],
        dataset_name=wss_hdf[1])

    # Construct tuple for storing wss
    wss = ()
    wss = wss + (np.array(wss_data[1]), )

    # Loop over all other time steps and add to wss
    for tstep in range(roi[0], roi[1]):
        # Extract the HDF location
        wss_hdf = time_steps[tstep + 1].find(
            'Attribute'
        ).find(
            'DataItem'
        ).text.split(':')

        # Print the name of the dataset
        print('\rReading {:s}:{:s}'.format(wss_hdf[0], wss_hdf[1]), end='')

        # Load the dataset
        wss_data = xdmf_in.hdf5_read(
            filename=wss_hdf[0],
            dataset_name=wss_hdf[1])

        wss = wss + (wss_data[1], )  # The new WSS dataset

    # Create one numpy array that holds all data
    wss = np.dstack(wss)

    # Newline
    print('\n')

    return wss


def compute_wss_metrics(wss):
    """ Used for extracting wss metrics from the wss data

    The metrics that are currently extracted are:
    • Oscillatory shear index (scalar)
    • Time average wss (vector)
    • Maximum wss (vector)
    • Minimum wss (vector)
    • Maximum and mean temporal wss gradient (Vector). Because no
        info about the timestep is available (it is assumed constant tough),
        normalization should occur manually
    • Relative residence time (RRT) (scalar)
    """

    # Compute WSS magnitude for each time step
    wss_mag = np.linalg.norm(wss, axis=1)
    wss_avg = np.average(wss, axis=2)

    # Compute the OSI.
    # The OSI is defined as: 0.5 * (1 - mag(average(tau))/ average(mag(tau)))

    denominator = np.average(wss_mag, axis=1)
    numerator = np.linalg.norm(wss_avg, axis=1)
    osi_vec = 0.5 * (1. - numerator / denominator)

    # Find the nodal minimum and the maximum wall shear stress magnitude
    index_max = np.argmax(wss_mag, axis=1)
    index_min = np.argmin(wss_mag, axis=1)

    # Compute minimum and maximum wss
    wss_max_vec = wss[range(wss.shape[0]), :, index_max]
    wss_min_vec = wss[range(wss.shape[0]), :, index_min]

    # Compute the twssg * dt
    twssg = np.diff(wss)

    # Compute the norm of all vectors
    twssg_mag = np.linalg.norm(twssg, axis=1)

    # Find the maximum value in time
    index_twssg_max = np.argmax(twssg_mag, axis=1)
    # Extract the maximum and mean twssg
    twssg_max_vec = twssg[range(twssg.shape[0]), :, index_twssg_max]
    twssg_mean_vec = np.average(twssg, axis=2)

    # Compute the twss magnitude gradient
    twssgm = np.diff(wss_mag)

    # Extract the maximum twssgm
    twssgm_max_vec = np.max(twssgm, axis=1)
    twssgm_mean_vec = np.average(twssgm, axis=1)

    # Compute the RRT (defined as the inverse of the numerator of the osi)
    rrt_vec = 1. / numerator


    # Construct dictionaries that can be written to xdmf for each metric
    osi = {
        'vector': osi_vec,  # Data vector
        'center': 'Node',  # Where the data is located (XDMF values only)
        'attribute_type': 'Scalar'  # This data is a scalar (XDMF values only)
    }

    rrt = {
        'vector': rrt_vec,  # Data vector
        'center': 'Node',  # Where the data is located (XDMF values only)
        'attribute_type': 'Scalar'  # This data is a scalar (XDMF values only)
    }

    wss_max = {
        'vector': wss_max_vec,  # Data vector
        'center': 'Node',  # Where the data is located (XDMF values only)
        'attribute_type': 'Vector'  # This data is a vector (XDMF values only)
    }

    wss_min = {
        'vector': wss_min_vec,  # Data vector
        'center': 'Node',  # Where the data is located (XDMF values only)
        'attribute_type': 'Vector'  # This data is a vector (XDMF values only)
    }

    # Compute the time averaged wall shear stress
    tawss = {
        'vector': wss_avg,  # Data vector
        'center': 'Node',  # Where the data is located (XDMF values only)
        'attribute_type': 'Vector'  # This data is a vector (XDMF values only)
    }

    twssg_max = {
        'vector': twssg_max_vec,  # Data
        'center': 'Node',  # Where the data is located (XDMF values only)
        'attribute_type': 'Vector'  # This data is a vector (XDMF values only)
    }

    twssgm_max = {
        'vector': twssgm_max_vec,  # Data vector
        'center': 'Node',  # Where the data is located (XDMF values only)
        'attribute_type': 'Scalar'  # This data is a vector (XDMF values only)
    }

    twssg_mean = {
        'vector': twssg_mean_vec,  # Data vector
        'center': 'Node',  # Where the data is located (XDMF values only)
        'attribute_type': 'Vector'  # This data is a vector (XDMF values only)
    }

    twssgm_mean = {
        'vector': twssgm_mean_vec,  # Data vector
        'center': 'Node',  # Where the data is located (XDMF values only)
        'attribute_type': 'Scalar'  # This data is a vector (XDMF values only)
    }

    # Construct a dictionary that contains all wss metrics
    wss_metrics = {
        'osi': osi,
        'rrt': rrt,
        'wss_max': wss_max,
        'wss_min': wss_min,
        'tawss': tawss,
        'twssg_max': twssg_max,
        # 'twssgm_max': twssgm_max,
        'twssg_mean': twssg_mean,
        # 'twssgm_mean': twssgm_mean,
    }

    return wss_metrics


def wss_analyze(ifile, ofile='wss_metrics.xdmf', t_min=0, t_max=0,
                compression='gzip'):
    """ Compute the oscillatory shear index

    ifile specifies the input file name
    """

    # Read xdmf file
    xdmf_in = xdmf.Xdmf(
        filename=ifile,
        mode='r'  # Read data
    )

    # Determine the range of interest
    xdmf_in.find_range(
        t_min=t_min,
        t_max=t_max)

    # Extract the wall shear stress
    wss = extract_wss(xdmf_in)

    # Close the hdf5 file
    xdmf_in.current_hdf5.close()

    # Compute the OSI, TAWSS, WSSmax and WSSmin
    wss_metrics = compute_wss_metrics(wss)

    # Write the OSI to an XDMF file

    xdmf.Xdmf(
        ofile,  # Output file
        'w',  # Write mode
        write_data=wss_metrics,  # The output vector
        topology=xdmf_in.topology_data,  # The topology
        geometry=xdmf_in.geometry_data,  # The geometry
        compression=compression  # Optional data compression
    )
