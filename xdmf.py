#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Module that handles reading and writing XDMF files

This module initializes the xmdf class that contains most functions for reading
and writing Xdmf files for OSI computations. The functionality can be extended
in the future """

# Import statements
import h5py  # Used for handling the heavy part of XDMF
import xml.etree.ElementTree as ET  # Used for handling the light part of XDMF
from xml.dom import minidom  # Used for writing the XDMF file
import numpy as np

__author__ = 'Sjeng Quicken work PC'
__email__ = 's.quicken@maastrichtuniversity.nl'
__date__ = '2018-04-23'

# Last Modified by:   Sjeng Quicken work PC
# Last Modified time: 2018-04-24


class Xdmf:
    """
    The Xdmf class contains most functionality to read and write XDMF files"""

    def __init__(self, filename, mode='r', write_data={}, topology=None,
                 geometry=None, compression=None):
        """Initializes the Xdmf class

        Xdmf file with name=filename is opened for either reading (mode = 'r')
        or writing (mode = 'w')"""

        self.filename = filename
        self.mode = mode  # Read: 'r' or write: 'w'
        self.current_hdf5 = None  # Holds currently opened hdf5 file
        self.write_data = write_data  # Possible data for writing

        if mode == 'w':
            # Write a file
            self.topology = topology
            self.geometry = geometry
            self.compression = compression  # For data compression
            self.nelem = self.topology[1].shape[0]  # Number of elements
            self.nnode = self.geometry[1].shape[0]  # Number of nodes
            self.write()

        elif mode == 'r':
            # Read the xdmf file
            self.light_read()

            # Read mesh
            self.read_mesh()
        else:
            raise ValueError(
                (
                    'Mode ''{:s}'' is not available'
                ).format(
                    self.mode)
            )

    def light_read(self):
        """Read and parse the light part (XML) of an XDMF file"""

        # Get the file tree
        tree = ET.parse(self.filename)
        root = tree.getroot()

        # Determine the XDMF version and check if compatible

        xdmf_version = root.attrib['Version']
        if xdmf_version != '3.0':
            raise Warning(
                (
                    'Possible incompatible XDMF version. ' +
                    'XDMF version is {:s}, not 3.0'
                ).format(
                    xdmf_version
                )
            )

        # Locate a FEniCS timeseries under Domain; Grid
        time_series = root.find('Domain').find('Grid')

        # Check if the data is actually a timeseries
        assert time_series.attrib['CollectionType'] == 'Temporal'

        # Find all time steps in the dataset
        time_steps = time_series.findall('Grid')
        ntime_steps = len(time_steps)

        # The first timestep should contain the mesh topology and geometry
        topology = time_steps[0].find('Topology')
        geometry = time_steps[0].find('Geometry')

        # Only HDF5 support is implemented at the time
        topo_data_type = topology.find('DataItem').attrib['Format']
        geom_data_type = geometry.find('DataItem').attrib['Format']

        # Check if data type is compatible
        assert topo_data_type == 'HDF' and geom_data_type == 'HDF'

        # Locate the the external HDF5 data
        topology_hdf = topology.find('DataItem').text.split(':')
        geometry_hdf = geometry.find('DataItem').text.split(':')

        # Add relevant info to self
        self.tree = tree
        self.xdmf_verion = xdmf_version
        self.time_steps = time_steps
        self.ntime_steps = ntime_steps
        self.topology = topology
        self.geometry = geometry
        self.topology_hdf = topology_hdf
        self.geometry_hdf = geometry_hdf

        return self

    def hdf5_read(self, filename, dataset_name):
        """
        Read a dataset from a HDF5 file"""

        # Check if the currently opened file can be used
        reuse = False
        if self.current_hdf5 is not None:
            if self.current_hdf5.filename == filename:
                # Reuse the currently opened HDF file
                reuse = True

        if not reuse:
            if self.current_hdf5 is not None:
                self.current_hdf5.close()

            # Open the file
            self.current_hdf5 = h5py.File(filename, 'r')

        # Read the dataset from the file
        dataset = self.current_hdf5[dataset_name][:]

        return self, dataset

    def read_mesh(self):
        """
        Read the mesh from HDF"""

        # Read the topology
        self.topology_data = self.hdf5_read(
            filename=self.topology_hdf[0],
            dataset_name=self.topology_hdf[1])

        # Read the geometry
        self.geometry_data = self.hdf5_read(
            filename=self.geometry_hdf[0],
            dataset_name=self.geometry_hdf[1])

        return self

    def find_range(self, t_min, t_max):
        """
        Find range of time series data
        """

        T = np.array([])

        for iiter in range(self.ntime_steps):
            # Loop over the time steps and extract the time
            T = np.append(
                T,
                float(
                    self.time_steps[iiter].find(
                        'Time').attrib['Value']
                )
            )

        # Find start and end of range
        self.data_roi = [
            np.argmin(np.abs(T - t_min)),
            np.argmin(np.abs(T - t_max))
        ]

    def create_xdmf_tree(self):
        """ Create the default xdmf tree"""

        # Define the root
        xdmf_file = ET.Element(
            'Xdmf',
            Version='3.0'
        )

        # Xdmf has subelement Domain
        domain = ET.SubElement(
            xdmf_file,
            'Domain'
        )

        # Domain has subelement Grid
        grid = ET.SubElement(
            domain,
            'Grid',
            Name='OscillatoryShearIndex',
            GridType='Uniform'
        )

        # Grid has subelements Topology, Geometry, Attribute
        topology = ET.SubElement(  # The elements
            grid,
            'Topology',
            NumberOfElements='{:d}'.format(
                self.nelem),  # Number of elements
            TopologyType='Triangle',  # OSI exists on surface
            NodesPerElement='3'
        )

        geometry = ET.SubElement(  # The Nodes
            grid,
            'Geometry',
            GeometryType='XYZ'
        )

        # topology, geometry and attributes all have DataItem
        data_item_topology = ET.SubElement(
            topology,
            'DataItem',
            Dimensions='{:d} 3'.format(
                self.nelem),  # Three dimensional data (triangles)
            NumbertType='UInt',  # Integer indices
            Format='HDF'
        )

        # Define the location of the topology data in the HDF file
        data_item_topology.text = (
            '{:s}:{:s}'.format(self.filename_hdf, '/topology'))

        data_item_geometry = ET.SubElement(
            geometry,
            'DataItem',
            Dimensions='{:d} 3'.format(
                self.nnode),  # Three dimensional data (XYZ)
            Format='HDF'
        )

        # Define the location of the geometry data in de HDF file
        data_item_geometry.text = (
            '{:s}:{:s}'.format(self.filename_hdf, '/geometry'))

        # Loop over the keys in write_data

        for ikey in self.write_data.keys():

            attribute_type = self.write_data[ikey]['attribute_type']
            center = self.write_data[ikey]['center']

            # Determine the dimension of the array
            dim1 = self.write_data[ikey]['vector'].shape[0]
            if self.write_data[ikey]['attribute_type'] == 'Scalar':
                # Scalar array has a secondary dimension of 1
                dim2 = 1
            elif self.write_data[ikey]['attribute_type'] == 'Vector':
                # Vector array has secondary dimension of 3
                dim2 = 3
            else:
                raise ValueError('Attribute not implemented ({:s})'.format(
                    self.write_data[ikey]['attribute_type'])
                )

            attribute = ET.SubElement(  # The OSI
                grid,
                'Attribute',
                Name=ikey,
                AttributeType=attribute_type,
                Center=center
            )

            data_item = ET.SubElement(
                attribute,
                'DataItem',
                Dimensions='{:d} {:d}'.format(
                    dim1, dim2),  # The dimension of the data array
                Format='HDF'
            )

            data_item.text = (
                '{:s}:/{:s}'.format(self.filename_hdf, ikey))

        tree = minidom.parseString(
            ET.tostring(xdmf_file)
        ).toprettyxml(indent='  ')

        self.tree = tree

        # The location of the osi data

    def write(self):
        """ Used to write data to an xdmf file"""

        # Define the HDF5 filename
        self.filename_hdf = self.filename.split('.')[0] + '.h5'

        # Build the element tree, a brief overview of the XML tree is available
        # from  xdmf_layout.txt

        self.create_xdmf_tree()

        # Write the xml file
        with open(self.filename, 'w') as f:
            f.write(
                self.tree
            )

        # Open the HDF file for writing
        self.current_hdf5 = h5py.File(self.filename_hdf, 'w')

        # Create the topology dataset
        self.current_hdf5.create_dataset(
            'topology',  # Topology data
            data=self.topology[1],
            compression=self.compression
        )

        # Create the geometry dataset
        self.current_hdf5.create_dataset(
            'geometry',  # Topology data
            data=self.geometry[1],
            compression=self.compression
        )

        # Loop over the keys in write_data
        for ikey in self.write_data.keys():

            vector = self.write_data[ikey]['vector']

            self.current_hdf5.create_dataset(
                ikey,  # Topology data
                data=vector,
                compression=self.compression
            )

        # Close the file
        self.current_hdf5.close()
